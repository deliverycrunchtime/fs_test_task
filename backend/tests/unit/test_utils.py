import pytest
from backend.src.utils import computeIoU


testdata = [
    ((2, 2, 5, 5), (3, 3, 6, 6), 0.286),
    ((2, 2, 5, 5), (6, 6, 10, 10), 0.0),
    ((2, 2, 5, 5), (2, 2, 5, 5), 1.0),
]

testdata1 = [
    ((2, 2, 5, 5), (3, 3, 6, 6), 1, 0.2),
    ((2, 2, 5, 5), (3, 3, 6, 6), 2, 0.28),
    ((2, 2, 5, 5), (3, 3, 6, 6), 5, 0.28571),
]


@pytest.mark.parametrize("truth, predict, expected", testdata)
def test_function(truth, predict, expected):
    assert computeIoU(truth, predict) == expected


@pytest.mark.parametrize("truth, predict,result_len, expected", testdata1)
def test_function_diff_len_res(truth, predict, result_len, expected):
    assert computeIoU(truth, predict, result_len)
