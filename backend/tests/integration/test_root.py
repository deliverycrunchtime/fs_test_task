from fastapi.testclient import TestClient
from backend.src.main import app
from backend.src.config import Settings
from backend.src.api_v1.base import get_settings


client = TestClient(app)

test_data = {
    "truth": {"left": 11, "top": 11, "right": 15, "bottom": 15},
    "predict": {"left": 12, "top": 12, "right": 17, "bottom": 15},
}

incorrect_data = {
    "not truth": {"left": 11, "top": 11, "right": 15, "bottom": 15},
    "predict": {"left": 12, "top": 12, "right": 17, "bottom": 15},
}


def get_settings_override():
    return Settings(result_length=5)


app.dependency_overrides[get_settings] = get_settings_override


def test_home():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == "Intersection over Union algorithm"


def test_predict_with_correct_data():
    response = client.post("/", json=test_data)
    assert response.status_code == 200
    assert response.json() == {"result": 0.40909}


def test_predict_with_incorrect_data():
    response = client.post("/", json=incorrect_data)
    assert response.status_code == 422
