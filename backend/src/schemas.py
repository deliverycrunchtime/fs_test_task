from pydantic import BaseModel, NonNegativeInt, validator


class Box(BaseModel):
    left: NonNegativeInt
    top: NonNegativeInt
    right: NonNegativeInt
    bottom: NonNegativeInt

    @validator("right")
    def left_le_right(cls, v, values):
        if values["left"] > v:
            raise ValueError("Right value should be >= than left.")
        return v

    @validator("bottom")
    def bottom_ge__top(cls, v, values):
        if values["top"] > v:
            raise ValueError("Bottom value should be >= than top.")
        return v
