def computeIoU(bbox1, bbox2, res_len=3):
    # (x1, y1, w1, h1) = bbox1
    (left1, top1, right1, bottom1) = bbox1
    # (x2, y2, w2, h2) = bbox2
    (left2, top2, right2, bottom2) = bbox2

    # Firstly, we calculate the areas of each box
    # by multiplying its height with its width.
    w1 = right1 - left1
    h1 = bottom1 - top1

    w2 = right2 - left2
    h2 = bottom2 - top2

    area1 = w1 * h1
    area2 = w2 * h2

    # Secondly, we determine the intersection
    # rectangle. For that, we try to find the
    # corner points (top-left and bottom-right)
    # of the intersection rectangle.
    inter_x1 = max(left1, left2)
    inter_y1 = max(top1, top2)
    inter_x2 = min(left1 + w1, left2 + w2)
    inter_y2 = min(top1 + h1, top2 + h2)

    # From the two corner points we compute the
    # width and height.
    inter_w = max(0, inter_x2 - inter_x1)
    inter_h = max(0, inter_y2 - inter_y1)

    # If the width or height are equal or less than zero
    # the boxes do not overlap. Hence, the IoU equals 0.
    if inter_w <= 0 or inter_h <= 0:
        return 0.0
    # Otherwise, return the IoU (intersection area divided
    # by the union)
    else:
        inter_area = inter_w * inter_h
        diff_area = float(area1 + area2 - inter_area)
        return float(format(inter_area / diff_area, f"0.{res_len}f"))
