from pydantic import BaseSettings


class Settings(BaseSettings):
    app_name: str = "IntersectionOverUnion"
    result_length: int

    class Config:
        env_file = ".env.dev"
        env_file_encoding = "utf-8"
