from functools import lru_cache
from fastapi import APIRouter, Depends
from ..schemas import Box
from ..utils import computeIoU
from ..config import Settings


@lru_cache()
def get_settings():
    return Settings()


router = APIRouter()


@router.get("/")
async def home():
    return "Intersection over Union algorithm"


@router.post("/")
async def predict(truth: Box, predict: Box,
                  settings: Settings = Depends(get_settings)):
    truth_box = truth.dict().values()
    predict_box = predict.dict().values()
    result = computeIoU(truth_box, predict_box, settings.result_length)
    return {"result": result}
