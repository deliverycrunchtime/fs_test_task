from fastapi import FastAPI, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware

from .api_v1.api import api_router

description = """
IntersectionOverUnion API measures the overlap
between two bounding boxes.A resulting value of 1 indicates perfect prediction,
while lower values suggest a poor prediction accuracy.
"""

app = FastAPI(title="IntersectionOverUnion", description=description)

origins = ["http://localhost", "http://localhost:8080"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    location = " > ".join(exc.errors()[0]["loc"])
    msg = exc.errors()[0]["msg"]
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content=jsonable_encoder(
            {"detail": f"Location:{location}.Message:{msg}", "body": exc.body}
        ),
    )


app.include_router(api_router)
