## Intersection over Union

IntersectionOverUnion one page website with python [fastapi](https://fastapi.tiangolo.com/) backend and [Vue.js](https://vuejs.org/) frontend. measures the overlap 
between two bounding boxes.A resulting value of 1 indicates perfect prediction, 
while lower values suggest a poor prediction accuracy.

## Installation

To run this application enter this command:

```commandline
docker-compose -f fs_home_task/docker-compose.yml up -d
```
This command start the server.

## Documentation
You can find API documentation [here](http://127.0.0.1:8000/redoc)(works only with running container)