.PHONY: ensure-pip dev lint test up down

all: ensure-pip dev lint test up

ensure-pip:
	pip install --upgrade pipenv pip
	pip --version
	pipenv --version

dev:
	pipenv install
	pipenv run pip install -e .
lint:
	pipenv run flake8 backend
test:  ## Run tests
	pipenv run python -m pytest

up:  ## Run Docker Compose services
	docker-compose -f docker-compose.yml up -d

down:  ## Shutdown Docker Compose services
	docker-compose -f docker-compose.yml down
